package net.strangled.chilini.utopiaengine;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Dave on 5/20/2015.
 */
public class Player {

    private HashMap<String, Counter> stores = new HashMap<>();
    private HashMap<String, Tool> toolBelt = new HashMap<>();
    private Counter hitPoints;
    private DoomsdayDevice device;

    public String name;

    // Array with names of the UEComponents in it
    protected String items[] = {"Lead", "Quartz", "Silica",
            "Gum", "Wax", "Silver"};

    public Player(String n) {

        // Set the player's name. If it is blank, set to Isodoros (Standard name)
        if (n.isEmpty()) {
            name = "Isodoros";
        } else {
            name = n;
        }

        hitPoints = new Counter("Hit Points", 6); // Set initial hit points
        device = new DoomsdayDevice();
        // Instantiate Counter for each item in inventory
        for (String item : items) {
            stores.put(item, new Counter(item, 4));
        }

        // Setup the toolbelt
        toolBelt.put("dowsingRod", new Tool("dowsingRod", "Dowsing Rod", "Up to -100 on any search"));
        toolBelt.put("paralysisWand", new Tool("paralysisWand", "Paralysis Wand", "+2 to each die in combat"));
        toolBelt.put("focusCharm", new Tool("focusCharm", "Focus Charm", "Add 2 energy points during activation"));


    }


    public void addToStores(String n, int i) {
        Counter store = stores.get(n);
        store.incCount(i);
    }

    public int checkStores(String n) {
        Counter store = stores.get(n);
        return store.getCount();
    }

    public HashMap<String, Integer> storesStatus() {
        HashMap<String, Integer> s = new HashMap<>();

        for (Map.Entry<String, Counter> entry : stores.entrySet()) {
            HashMap<String, Integer> h = new HashMap<>();
            h.put(entry.getKey(), entry.getValue().getCount());
        }

        return s;
    }

    /**
     * Created by chili_000 on 5/19/2015.
     */

    /* This is a counter class. You can define max and min for values on creation.
    Defaults:
     - Min: 0
     */
    public static class Counter {

        private int    countCur;  // Current count
        private int    countMin;  // Minimum value
        private int    countMax;  // Maxiumum value
        private String countName; // Name of counter

        public Counter() throws ExceptionInInitializerError {
            throw new ExceptionInInitializerError("This method requires parameters!");
        }

        // Method to initialize counter with minimum at 0
        public Counter(String name, int max) {
            countMin = 0;
            countMax = max;
            countName = name;
        }
        // Method to initialize counter with defined min and max
        public Counter(String name, int min, int max) {
            countMin = min;
            countMax = max;
            countName = name;
        }

        // Method to initialize counter with defined min and max with start count defined
        public Counter(String name, int min, int max, int start) {
            countCur  = start;
            countMin  = min;
            countMax  = max;
            countName = name;
        }

        public int getCount() {
            return countCur;
        }

        private void setCount(int c) {
            countCur = c;
        }

        public int decCount() {
            if (getCount() == 0) {
                return -1;
            } else {
                setCount(countCur - 1);
                return countCur;
            }
        }

        public int decCount(int n) {
            if (getCount() == 0 || (getCount() - n) < 0 ) {
                return -1;
            } else {
                setCount(countCur - n);
                return countCur;
            }
        }

        public int incCount() {
            if (getCount() == countMax ) {
                return -1;
            } else {
                setCount(countCur + 1);
                return countCur;
            }
        }

        public int incCount(int n) {
            if (getCount() == countMax || (getCount() + n) > countMax) {
                return -1;
            } else {
                setCount(countCur + n);
                return countCur;
            }
        }

    }
}