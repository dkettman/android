package net.strangled.chilini.utopiaengine;

/**
 * Created by Dave on 5/20/2015.
 */
public class Tool {

    private String  shortName; // Short name of tool
    private String  longName;  // Long name of tool
    private String  desc;      //What it does
    private boolean charged;   //Is it charged?

    public Tool (String s, String l, String d) {
        this.shortName = s;
        this.longName = l;
        this.desc = d;
        this.charged = true;
    }

    void useTool() {
        this.charged = false;
    }

    boolean isCharged() {
        return this.charged;
    }
}
