package net.strangled.chilini.utopiaengine;

/**
 * Created by Dave on 5/20/2015.
 */
public class DoomsdayDevice {

    private Player.Counter godsHand = new Player.Counter("God's Hand",6); // God's Hand energy level
    private Player.Counter skulls   = new Player.Counter("Skulls", 8); // Number of skulls left
    private Player.Counter timeTrack = new Player.Counter("Time Track", 22);

    private int eventDays[] = {2, 5, 8, 11, 14, 17, 20};

    int getDay() {
        return this.timeTrack.getCount();
    }

    boolean isEventDay() {
        for (int i = 0; i < this.eventDays.length; i++) {
            if ( this.eventDays[i] == this.getDay())
                return true;
        }
        return false;
    }

    int daysLeft() {
        return this.getDay() - this.skulls.getCount();
    }

    boolean canRemoveSkull() {
        if ( this.godsHand.getCount() >= 3 ) {
            return true;
        } else {
            return false;
        }
    }

    void removeSkull() {
        if ( this.canRemoveSkull() ) {
            this.skulls.decCount();
        }
    }

    void incEnergy(int e) {
        this.godsHand.incCount(e);
    }

    void decEnergy(int e) {
        this.godsHand.decCount(e);
    }
}
